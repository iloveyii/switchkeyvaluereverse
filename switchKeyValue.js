/**
 * Assignment - Assumptions
 * 1 - All primitive data types should be swapped and reversed
 *     (boolean, number, string)
 * 2 - All objects data types should be swapped and reversed
 *     (null, object literal, array
 * 3 - All other
 *     ( empty string and undefined should be swapped and reversed)
 *     ( function should remain intact )
 */


/**
 * This function reverses the string characters - 'person' would become 'nosrep'
 * #1 - First split the string to get it into array
 * #2 - Apply reverse function of the array (from 1)
 * #3 - Use join function of the array to get they array in string format
 *
 * @param str
 * @returns {string}
 */
function reverseString(str) {
    let strData = str;
    // if number then convert to string first so we can use split function
    if( ! isNaN(strData)) {
        strData = str + "";
    }
    // # 1
    const strArray = strData.split("");
    // # 2
    const strArrayReversed = strArray.reverse();
    // # 3
    const strReversed = strArrayReversed.join("");

    return strReversed;
}

/**
 * This function swaps the key value of the object recursively
 * It also reverses the value (string) using the above function reverseString
 * If the value is not primitive type then it calls itself recursively
 * @param obj
 */
function swapKeyValueReverse(obj) {
    const keys = Object.keys(obj);
    const newObj = {};
    keys.forEach(function (k) {
        if(typeof obj[k] === 'function') { // functions intact
            newObj[k] = obj[k];
        } else if( (typeof obj[k] === 'object') && (obj[k] !== null) ) { // if obj but no null do recursion
            newObj[k] = swapKeyValueReverse(obj[k]);
        } else { // primitive data types
            newObj[obj[k]] = reverseString(k);
        }
    });

    return newObj;
}

module.exports = {
    reverseString : reverseString,
    swapKeyValueReverse : swapKeyValueReverse,
};

const testObj = {
    "person" : "beans",
    "present" : false,
    "room" : 1412,
    15 : "Beckom",
    array : [ 1, 2, 3, 4],
    wow: null,
    und : undefined,
    empty: "",
    func : function () {},
    level1 : {
        level2: 'val2',
        level21: {
            level3: 'val3',
            level31: {
                level4: 'val4'
            }
        }
    }
};
// const result = swapKeyValueReverse(testObj);
// console.log(result);
