const assert = require('chai').assert;
const expect = require('chai').expect;
const switchKeyValue = require('../switchKeyValue');

// Results
const reverseStringResult = switchKeyValue.reverseString('person');
const obj = { "person":"beans"};
const swapKeyValueResult = switchKeyValue.swapKeyValueReverse(obj);

describe('switchKeyValue', function () {
    describe('Testing reverseString:', function () {
        it('reverseString should return reverse of person which is nosrep', function () {
            assert.equal(reverseStringResult, 'nosrep');
        });
        it('reverseString should return type string', function () {
            assert.typeOf(reverseStringResult, 'string');
        })
    });

    describe('Testing swapKeyValueReverse:', function () {
        it('swapKeyValueReverse should have a property beans', function () {
            expect(swapKeyValueResult).to.have.property('beans');
        });
        it('swapKeyValueReverse should have a property beans with value nosrep', function () {
            expect(swapKeyValueResult).to.have.property('beans', 'nosrep');
        })
    })
});


// TEST Drive
// npm run test
