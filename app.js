const fs = require('fs');
const swapKeyValueReverse = require('./switchKeyValue').swapKeyValueReverse;
const fileNames = process.argv.splice(2);

fileNames.forEach(function (fileName) {
    fs.readFile(fileName, 'utf-8', function(error, data) {
        if(error) throw error;
        const newObj = swapKeyValueReverse(JSON.parse(data));
        const newObjString = JSON.stringify(newObj);
        fs.writeFile('output.json', newObjString, 'utf-8', function (err) {
            if(err) throw err;
            console.log('Wrote output to output.json successfully');
        });
        console.log(newObjString);
    });
});

// Test Drive
// node app.js data.json [data.json, ..]
