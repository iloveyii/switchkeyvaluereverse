Test - Switch object key value
===============================

Write a javascript program that as input takes a JSON object and returns it with all key/values switched and the (new) value reversed as a string.

```
 {"person": "beans"}
```
should output
```
{"beans":"nosrep"}
```

Please be mindful of the nested nature and the different data types that JSON offers! I'll leave how input/output works up to you!


DIRECTORY STRUCTURE
-------------------

```
/
    app.js                    should be run in node, it reads file name from cmd
    data.json                 it contains sample test data
test
    switchKeyValueTest.js     is the test file
```

INSTALLATION
-------------------

```
 npm install
 npm run test
 node app.js data.json
```

SOLUTION
-------------------

The solution is developed in JS, node, mocha, chai. The input should be give in a json file to the app.js. For example run it for a sample json data.json use :

```
 node app.js data.json
```
